import './css/index.css'
import Counter from './components/Counter';
import {BrowserRouter, Routes, Route} from 'react-router-dom'
import Table from './components/Table';
import NavBar from './components/NavBar';
import Home from './components/Home'
const App = () => (
    <BrowserRouter>
    <NavBar/>
    <Routes>
        <Route path='/' element={<Home/>}/>
        <Route path='/counter' element={<Counter/>}/>
        <Route path='/table' element={<Table/>}/>
    </Routes>    
    </BrowserRouter>
);

export default App;