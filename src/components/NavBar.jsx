import {Link} from 'react-router-dom'

const NavBar = () => {
  return (
    <div>
        <ul>
            <li><Link to='/' style={{ textDecoration: 'none' }}><h2>Patricio</h2></Link></li>
            <li><Link to='/counter' style={{ textDecoration: 'none' }}><h2>Counter</h2></Link></li>
            <li><Link to='/table' style={{ textDecoration: 'none' }}><h2>Table</h2></Link></li>
        </ul>
    </div>
  )
}

export default NavBar