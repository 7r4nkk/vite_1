import useCount from "../hooks/UseCount";

const Counter = () => {
    const {counter, oneMore, oneLess} = useCount()
    return(
        <div className="card main">
            <input typeof="number" value={counter}></input>
            <div className="buttons">
                <button className="more" onClick={oneMore}>+</button>
                <button className="less" onClick={oneLess} disabled={counter <= 0}>-</button>
            </div>
        </div>
    );
}

export default Counter;