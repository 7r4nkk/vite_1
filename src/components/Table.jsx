import MUIDataTable from "mui-datatables";
import useUsers from "../hooks/useUsers";

const options = {
  filterType: 'checkbox',
};
const columns = [
  {
      name: "name",
      label: "NAME"
  },
  {
      name: "status",
      label: "STATUS"
  },
  {
      name: "email",
      label: "EMAIL"
  }
]
const Table = () => {
  const {users} = useUsers()
  return (
    <div>
        <MUIDataTable
        title={"USERS"}
        data={users}
        columns={columns}
        options={options}
      />
    </div>
  )
}

export default Table



