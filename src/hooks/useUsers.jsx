import { useState, useEffect } from "react";

const useUsers = () => {
    const [users, setUsers] = useState( [] )

    const URL = 'https://gorest.co.in/public/v2/users'
    const showData = async () => {
      const response = await fetch(URL)
      const data     = await response.json()
      setUsers(data)
    }
    useEffect( ()=>{
      showData()
    }, [])
    return{
        users
    }
}

export default useUsers;
