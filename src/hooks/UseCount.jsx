import { useState } from "react";

const useCount = () => {
    const [counter, setCounter] = useState(0)

    const oneMore = () =>{
        setCounter(counter + 1)
    }
    const oneLess = () =>{
        setCounter(counter - 1)
    }
    return{
        counter,
        oneMore,
        oneLess
    }
}

export default useCount;